<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/sh', function () {
    return view('blog.category');
});

Route::get('/', 'BlogController@index')->name('index');
Route::get('/show/{id}/{slug}', 'BlogController@show')->name('show');
Route::get('/categories/{id}', 'BlogController@category')->name('category');
Route::get('/komen', 'CommentController@index')->name('comment.index');
Route::post('/show/{id}', 'CommentController@store')->name('comment.store');

Route::middleware('auth')->group(function () {

    Route::get('/admin', 'HomeController@index');


    Route::prefix('/admin')->name('admin.')->group(function () {

        Route::resource('/categories', 'CategoryController');

        Route::resource('/articles', 'ArticleController');

        Route::resource('/users', 'UserController');
    });
});

Route::get('/login', 'AuthController@login');
Route::post('/login', 'AuthController@loginProccess');
Route::get('/register', 'AuthController@register');
Route::post('/register', 'AuthController@registrationProccess');
Route::post('/logout', 'AuthController@logout');
