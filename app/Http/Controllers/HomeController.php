<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Article;
use App\Model\Category;
use App\Model\User;
use App\Model\Tag;

class HomeController extends Controller
{
    public function index()
    {
        $articles = Article::all();
        $categories = Category::all();
        $users = User::all();
        $tags = Tag::all();
        return view('admin.index', compact('articles', 'categories', 'users', 'tags'));
    }
}
