<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Article;
use App\Model\Category;
use App\Model\User;
use App\Model\Tag;
use App\Model\Comment;

class BlogController extends Controller
{
    public function __construct()
    {
        $this->model = new Article;
    }

    public function index()
    {
        $hits = $this->model->orderBy('counter', 'desc')->take(6)->get();
        $recens = $this->model->orderBy('id', 'desc')->take(5)->get();
        $posts = $this->model->orderBy('created_at', 'desc')->paginate(10);
        $categories = Category::all();

        return view('blog.index', compact(['hits', 'recens', 'posts', 'categories']));
    }

    public function show($id)
    {
        $categories = Category::all();
        $post = $this->model->find($id);
        $recens = $this->model->orderBy('id', 'desc')->get();
        $comments = Comment::orderBy('created_at', 'desc')->where('article_id', $id)->get();

        if ($post) {
            $post->increment('counter');
        }

        return view('blog.show', compact(['post', 'recens', 'comments', 'categories']));
    }

    public function category($id)
    {
        $categories = Category::all();
        $category = Category::where('id', $id)->first();
        $recens = $this->model->orderBy('id', 'desc')->take(5)->get();
        $articles = Article::where('category_id', $id)->paginate(10);

        return view('blog.category', compact('categories', 'category', 'articles', 'recens'));
    }
}
