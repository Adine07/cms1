<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\User;
use App\Model\Article;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    function __construct()
    {
        $this->model = new User();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny', $this->model);
        $users = $this->model->all();

        return view('admin.user.index', compact('users'));
    }

    public function show($id)
    {
        $user = $this->model->find($id);
        $this->authorize('view', $user);

        return view('admin.user.profile', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', $this->model);
        return view('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', $this->model);
        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|max:255',
            'password' => 'required|max:255',
            'role' => 'required',
        ]);

        $endcriptPassword = Hash::make($request->password);

        $request->merge([
            'password' => $endcriptPassword,
        ]);

        User::create($request->all());

        return redirect('/admin/users');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->model->find($id);
        $this->authorize('update', $user);

        return view('admin.user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = $this->model->find($id);
        $this->authorize('update', $user);

        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|max:255',
            'role' => 'required',
        ]);

        $request->merge(['password' => $user->password]);
        $user->update($request->all());
        return redirect()->route('admin.users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = $this->model->find($id);
        $this->authorize('delete', $user);

        if ($user->id != 1) {
            Article::where('user_id', $id)->update([
                'user_id' => 1,
            ]);

            if (Auth::user()->role == 'admin') {
                $user->delete();
                return redirect()->back();
            } else {
                Auth::logout();
                $user->delete();
                return redirect('/login');
            }
        } else {
            return redirect()->back();
        }
    }
}
