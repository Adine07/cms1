<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Article;
use App\Model\Category;
use App\Model\User;
use App\Model\Tag;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class ArticleController extends Controller
{
    function __construct()
    {
        $this->model = new Article();
        $this->imgPath = public_path('img');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny', $this->model);

        if (auth()->user()->role == 'author') {
            $articles = $this->model->where('user_id', auth()->user()->id)->orderBy('created_at', 'desc')->paginate(10);
        } else {
            $articles = $this->model->orderBy('created_at', 'desc')->paginate(10);
        }

        return view('admin.article.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', $this->model);

        $categories = Category::all();
        $tags = Tag::all();
        return view('admin.article.create', compact('categories', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', $this->model);

        $request->validate([
            'title' => 'required|max:255',
            'category_id' => 'required',
            'content' => 'required',
            'photo' => 'required',
            'tag' => 'required',
        ]);

        if ($request->photo) {
            $request = $this->uploadImage($request);
        }

        $request->merge([
            'user_id' => Auth::user()->id,
            'slug' => Str::of($request->title)->slug('-'),
        ]);

        $article = $this->model->create($request->all());;


        if ($article) {
            $tagnames = $request->tag;
            foreach ($tagnames as $tagname) {
                $tag = Tag::firstOrCreate(['name' => $tagname]);
            }
            $pico = Tag::whereIn('name', $tagnames)->get()->pluck('id');
            $article->Tag()->sync($pico);
        }

        return redirect('/admin/articles');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $articles = $this->model->find($id);
        $this->authorize('update', $articles);
        $categories = Category::all();
        $tags = Tag::all();

        return view('admin.article.edit', compact('articles', 'categories', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required|max:255',
            'category_id' => 'required',
            'content' => 'required',
            'user_id' => 'required',
            'tag' => 'required',
        ]);

        $model = $this->model->find($id);
        $this->authorize('update', $model);

        if ($request->photo) {
            $this->removeImage($model->image);
            $request = $this->uploadImage($request);
        }

        $article = $this->model->find($id);

        if ($article) {
            $tagnames = $request->tag;
            foreach ($tagnames as $tagname) {
                $tag = Tag::firstOrCreate(['name' => $tagname]);
            }
            $pico = Tag::whereIn('name', $tagnames)->get()->pluck('id');
            $article->Tag()->sync($pico);
        }

        $this->model->find($id)->update($request->all());

        return redirect('/admin/articles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = $this->model->find($id);

        $this->authorize('delete', $model);
        $this->removeImage($model->image);

        $model->delete();

        return redirect('/admin/articles');
    }

    // Untuk Image

    public function uploadImage($request)
    {
        $img = $request->file('photo');
        $newName = time() . '.' . $img->getClientOriginalExtension();

        $img->move($this->imgPath, $newName);

        $request->merge([
            'image' => $newName,
        ]);

        return $request;
    }

    public function removeImage($img)
    {
        $fullPath = $this->imgPath . '/' . $img;

        if ($img && file_exists($fullPath)) {
            unlink($fullPath);
        }
    }

    public function handleTag(Request $request, Article $article)
    {
        $tagnames = $request->tag;
        foreach ($tagnames as $tagname) {
            Tag::firstOrCreate(['name' => $tagname])->save();
        }
        $tags = Tag::whereIn('name', $tagnames)->get()->pluck('id');
        $article->Tag()->sync($tags);
    }
}
