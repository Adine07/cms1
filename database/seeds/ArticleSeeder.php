<?php

use Illuminate\Database\Seeder;
use App\Model\Article;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Article::truncate();

        Article::create([
            'title' => 'Dear Messi, Cristiano Ronaldo Cabut dari Madrid dengan Damai',
            'user_id' => '1',
            'category_id' => '4',
            'content' => 'barcelona -

            Jika Lionel Messi memang hendak meninggalkan Barcelona, ada baiknya mencontoh Cristiano Ronaldo. CR7 meninggalkan Real Madrid dengan damai.
            
            Lionel Messi kembali jadi perbincangan. Bukan karena aksi liak-liuknya di lapangan, tapi perihal masa depannya di Barcelona.
            
            Musim 2019/2020 kemarin jadi catatan buruk baginya. Messi tak bisa memenangi gelar apapun (seperti halnya di tahun 2007/2008), ribut-ribut dengan staff pelatih, hingga bersitegang dengan para petinggi klub.
            
            Alhasil di bursa transfer musim panas ini, Lionel Messi kabarnya bisa jadi meninggalkan Barcelona. Apalagi, pemain asal Argentina itu belum membubuhkan tanda tangan perpanjangan kontrak yang habis di tahun 2021.

            Malah kabar terbaru, Lionel Messi sudah bertemu dengan pelatih baru Barcelona, Ronald Koeman. Pertemuan itu pun belum menemui titik terang si pemain, eh malah bocor ke publik dan Messi jadinya geram.
            
            Marca, media asal Spanyol membandingkan jika Lionel Messi meninggalkan Barcelona maka ada baiknya mencontoh Cristiano Ronaldo kala meninggalkan Real Madrid di tahun 2018 lalu. Ronaldo mampu meninggalkan Los Blancos dengan damai.

            Memang sih, Cristiano Ronaldo dikabarkan kecewa dengan pihak klub karena tak bisa menaikkan gajinya. Tapi nyatanya, kepindahan pemain asal Portugal ke Juventus itu tidak meninggalkan drama berkepanjangan.

            Cristiano Ronaldo meninggalkan Real Madrid dengan raihan trofi Liga Champions. Dia pun punya hubungan yang baik dengan pelatihnya, Zinedine Zidane.
            
            Cristiano Ronaldo pun tidak mempunyai konflik dengan para pemain Real Madrid lainnya, para fans, atau dengan petinggi klub. Malah, dirinya kembali datang ke Santiago Bernabeu untuk menyaksikan El Clasico di bulan Maret kemarin.

            Maka disarankan, Lionel Messi juga bisa meninggalkan Barcelona jika hal itu sungguh terjadi. Tidak ada konflik atau drama lainnya, sehingga tidak ada "noda" dalam perjalanan hidupnya selama membela Barcelona.',
            'image' => '1111111111.jpeg',
            'slug' => 'Dear-Messi'
        ]);

        Article::create([
            'title' => 'Top Skor Liga Europa: Bruno Fernandes',
            'user_id' => '2',
            'category_id' => '4',
            'content' => 'Jakarta -

            Bruno Fernandes berhak atas gelar top skor Liga Europa musim ini. Jumlah gol pemain Manchester United itu tak bisa disalip oleh Romelu Lukaku-nya Inter Milan.
            
            Lukaku sebenarnya berpeluang mengejar gol Bruno Fernandes seiring dengan penampilannya di final Liga Europa, Sabtu (22/8/2020) dini hari WIB. Ia pun mengawali laga itu dengan menjanjikan usai menjebol gawang Sevilla di menit kelima.
            
            Namun, setelah itu Romelu Lukaku tak kuasa mencetak gol lagi buat Inter Milan. Penyerang asal Belgia itu malah menjebol gawang sendiri untuk menentukan kemenangan Sevilla -- dan tentu saja gol bunuh diri tidaklah dihitung.
            
            Dengan tambahan satu gol di final Liga Europa itu, Romelu Lukaku tercatat bikin tujuh gol untuk berada di posisi kedua daftar top skor Liga Europa musim 2019/2020. Ia terpaut satu gol dari Bruno Fernandes.

            Sebagai top skor Liga Europa musim ini, Bruno Fernandes tercatat melakukannya bersama dua klub. Pada paruh musim pertama ia tampil di Liga Europa bersama Sporting dan kemudian berkpirah untuk Manchester United di paruh kedua musim.

            Berikut daftar top skor Liga Europa:

            8 Bruno Fernandes (Sporting CP/Manchester United)

            7 Romelu Lukaku (Inter Milan)

            6 Andraž Šporar (Slovan Bratislava/Sporting CP)
            6 Daichi Kamada (Eintracht Frankfurt)
            6 Alfredo Morelos (Rangers)
            6 Edin Višća (İstanbul Başakşehir)
            6 Diogo Jota (Wolves)

            5 Munir (Sevilla)
            5 Mason Greenwood (Manchester United)
            5 Fabian Frei (Basel)
            5 Marko Raguz (LASK)
            
            Selain jadi top skor Liga Europa, nama Bruno Fernandes juga kembali muncul dalam jajaran atas daftar top assist Liga Europa musim 2019/2020, sekalipun bukan di posisi pertama.

            Empat assist yang dibuat oleh Bruno Fernandes masih di bawah Juan Mata (lima assist), rekan satu timnya di Manchester United, dan Galeno (Braga, enam assist) yang menjadi top assist Liga Europa musim ini.',
            'image' => '1111111112.jpeg',
            'slug' => 'Top-Skor-Liga-Europa'
        ]);

        Article::create([
            'title' => 'Apple Watch Series 3 dan 5 Jadi Smartwatch Terlaris',
            'user_id' => '3',
            'category_id' => '2',
            'content' => 'Jakarta -

            Apple semakin mendominasi ranah wearable, tepatnya jam tangan pintar, setelah dua seri Apple Watch didaulat jadi smartwatch paling laris selama pertengahan pertama 2020.
            
            Menurut data dari Counterpoint, ada dua seri Apple Watch yang ada dalam daftar lima smartwatch paling laris selama pertengahan pertama 2020. Yaitu Apple Watch Series 5 yang ada di posisi ke-1 dan Apple Watch Series 3 di posisi ke-2.
            
            Selama periode tersebut, pemasukan Apple dari Apple Watch mendominasi pemasukan secara total yang dihasilkan oleh perangkan sejenis. Yaitu lebih dari setengah pemasukan total dipegang oleh Apple, meningkat 43,2% dibanding periode yang sama tahun sebelumnya.
            
            Jumlah pengapalan Apple Watch pun meningkat 22% secara year on year, bahkan saat pengapalan secara total di industri ini relatif rata akibat pandemi Corona, demikian dikutip detikINET dari Phone Arena, Jumat (21/8/2020).
            
            Dalam daftar smartwatch paling laris selama H1 2020, di bawah dua seri Apple Watch ada Huawei Watch GT 2, Samsung Galaxy Watch Active 2, dan Imoo Z3 4G. Namun ada hal unik terkait pangsa pasar dari segi pemasukan untuk ranah ini.

            Apple memang masih memegang pangsa pasar lewat Apple Watch dengan raihan sebesar 51,4%, dan di posisi dua ada gabungan dari berbagai perusahaan wearable lain. Namun di posisi ke-3 menariknya ada Garmin, yang populer dengan lini wearable Forerunner dan Fenix, yang dikhususkan untuk kegiatan olah raga.

            Barulah di bawah Garmin, secara berturut-turut, ada Huawei, Samsung, Imoo, Amazfit, Fitbit, dan Fossil. Dari daftar ini ada beberapa nama yang terlihat punya pertumbuhan cukup besar.

            Contohnya adalah Amazfit, yang berhasil menyalip Fitbit yang sudah ada lebih dulu, dan juga Imoo, yang cukup populer di Indonesia namun tak terlalu dikenal di negara-negara Barat.',
            'image' => '1111111113.jpeg',
            'slug' => 'Apple-Watch'
        ]);

        Article::create([
            'title' => 'Bocoran Pixel 5: Dua Kamera Belakang dan Sensor Sidik Jari',
            'user_id' => '1',
            'category_id' => '2',
            'content' => '<p>Jakarta -

            Setelah merilis Pixel 4A, Google juga bakal merilis Pixel 5 dan Pixel 4A 5G. Bocoran terbaru memperlihatkan Pixel 5 punya dua kamera belakang, salah satunya ultrawide, dan sebuah sensor sidik jari di belakang ponsel.</p>
            
            <p>
            Bocoran gambar render ini diposting oleh @OnLeaks di akun Twitternya. Dari bocoran tersebut, terlihat modul kamera belakangnya tersimpan dalam bidang berbentuk persegi seperti Pixel 4.
            </p>

            <p>
            Tak ada hal yang mengejutkan dari bocoran gambar ini, kecuali penampakan sensor sidik jari di belakang ponsel. Pasalnya di Pixel 4, Google tak lagi membenamkan sensor sidik jari dan hanya mengandalkan face recognition lewat sensor yang tersimpan di atas layar.
            </p>

            <p>
            Sementara di render gambar Pixel 5, "wajah" ponsel hampir dipenuhi oleh layar kecuali sebuah hole punch untuk kamera depan di bagian kanan atas layar, demikian dikutip detikINET dari The Verge, Sabtu (22/8/2020).
            </p>

            Memang, keaslian hasil render ini sulit untuk ditebak. Mengingat sejauh ini Google baru memperlihatkan bagian pinggir ponsel Pixel 5 dan Pixel 4A 5G, dan keduanya memang terlihat identik.

            Soal ukuran layar, OnLeaks menyebut Pixel 5 bakal punya layar antara ,57 sampai 5,8 inch. Namun Android Central menyebut Pixel 5 bakal menggunakan layar 6 inch OLED dengan refresh rate 90Hz.

            Sejumlah spesifikasi lain yang sejauh ini sudah beredar antara lain adalah prosesor Snapdragon 765G yang mendukung 5G, RAM 8GB, dan storage 128GB. Sementara kapasitas baterainya belum diketahui, namun kabarnya Pixel 5 bakal mendukung Qi wireless charging 15W dan reverse wireless charging 5W.

            Penggunaan Snapdragon 765G ini -- jika benar -- membuatnya bakal kalah kencang dibanding pendahulunya. Pixel 4 menggunakan prosesor flagship Snapdragon 855, sementara Snapdragon 765G adalah prosesor kelas menengah.

            Kemungkinan penggunaan Snapdragon 765G ini dilakukan untuk menekan harga dan tetap bisa memberikan konektivitas 5G di Pixel 5.',
            'image' => '1111111114.jpeg',
            'slug' => 'Bocoran-Pixel-5'
        ]);

        Article::create([
            'title' => 'Bikin Laper! Empuk Juicy Iga Bakar Saus BBQ yang Tebal',
            'user_id' => '2',
            'category_id' => '3',
            'content' => 'Jakarta -

            Bagi penggemar iga bakar, wajib coba iga bakar rekomendasi dari tim Bikin Laper yang satu ini. Daging iga yang empuk dan juicy bikin nagih terus.
            
            Masih di sekitar Bekasi, petualangan tim Bikin Laper terus berlanjut. Kali ini petualangan dipandu oleh Ncess Nabati yang ditemani Trio Daster. Di sana mereka mampir ke restoran Kedai Rakyat yang ada di Jatibening.
            
            "Restoran ini terkenal banget dengan menu iga bakarnya. Lihat nih guys penampilan iga bakarnya," jelas Ncess yang sudah tidak sabar mencicipi menu spesial ini.

            Satu porsi Iga Bakar dihargai sekitar Rp 45.000. Potongan daging iganya cukup besar, dengan bumbu BBQ yang melimpah dan meresap hingga ke bagian tulang.

            "Ini iganya pakai saus BBQ, dagingnya tebel-tebel banget. Guys parah, aku suka banget bakarannya sempurna. Tidak terlalu kegosongan, tapi tekstur dagingnya lembut banget. Tidak ada aroma tak sedap atau smelly, ini benar benar endol surendol banget," puji Ncess ketika mencicipi iga bakar super empuk ini.

            Bagian tengah iga bakar terdapat lemak yang membuat rasanya semakin juicy dan gurih. Paduan kecap dan saus BBQ yang melimpah membuat rasa daging iga semakin enak.

            "Ini parah enak banget, daging iganya lembut sekali,"komen Trio Daster di tengah sesi makan mereka.

            Bagi yang penasaran dengan petualangan kuliner selanjutnya, bisa tonton terus acara Bikin Laper. Tayang setiap hari di Trans TV pukul 17.00. Jangan sampai ketinggalan ya!',
            'image' => '1111111115.jpeg',
            'slug' => 'Bikin-Laper'
        ]);

        Article::create([
            'title' => 'Apa Benar Sarapan Nasi Bikin Gemuk? Cek 5 Fakta Soal Nasi Ini',
            'user_id' => '3',
            'category_id' => '3',
            'content' => 'Jakarta -

            Banyak orang yang selalu sarapan dengan nasi dan lauk. Sementara ada juga yang menyebut bahwa sarapan nasi bikin gemuk. Ini faktanya.
            
            Nasi merupakan makanan pokok orang Indonesia. Bahkan orang Indonesia selalu beranggapan kalau tidak makan nasi namanya belum makan. Makan nasi memang mengenyangkan, tetapi jika tidak sesuai anjuran, makan nasi bisa menyebabkan gangguan kesehatan, seperti diabetes hingga kegemukan.
            
            Kegemukan merupakan masalah yang paling banyak dialami. Karenanya, makan nasi juga ada aturannya. Seperti waktu makan nasi yang terbaik, jenis nasi, jumlah atau porsi per kali santap.
            
            Sarapan nasi sudah dilakukan orang Indonesia dan Asia sejak berabad silam. Karenanya simak 5 fakta soal nasi ini agar bisa mengonsumsi dengan takaran pas dan waktu yang tepat.
            
            Makan nasi bisa mengakibatkan dua dampak, yaitu dampak positif dan dampak negatif. Hal itu tergantung dengan seberapa banyak porsi makan nasi. Makan nasi memang menyehatkan. Kandungan karbohidrat di dalamnya bisa memberikan energi.

            Namun, jika dimakan dalam porsi yang banyak karbohidrat tersebut akan berubah menjadi gula yang bisa memicu penyakit diabetes. Dilansir dari Cosmopolitan, porsi makan nasi bisa disesuaikan dengan kebutuhan.

            Misalnya wanita yang ingin mempertahankan berat badan bisa makan nasi sebanyak 104 gram per porsi. Untuk wanita yang ingin menurunkan berat badan dianjurkan makan nasi 37 gram per porsi. Wanita dan pria memiliki porsi yang berbeda.

            Untuk pria yang ingin mempertahankan berat badan bisa makan hingga 130 gram per porsi. Sementara untuk pria yang ingin menurunkan berat badan bisa makan sebanyak 50 gram nasi per porsi. Baik wanita dan pria dianjurkan tidak makan nasi lebih dari tiga porsi dalam sehari.',
            'image' => '1111111116.jpeg',
            'slug' => 'Sarapan-Nasi-Bikin-Gemuk'
        ]);
    }
}
