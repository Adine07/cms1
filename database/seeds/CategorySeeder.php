<?php

use Illuminate\Database\Seeder;
use App\Model\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::truncate();

        Category::create([
            'name' => 'Default',
        ]);

        Category::create([
            'name' => 'Tekhnologi',
        ]);

        Category::create([
            'name' => 'Food',
        ]);

        Category::create([
            'name' => 'Sport',
        ]);

        Category::create([
            'name' => 'Politic',
        ]);

        Category::create([
            'name' => 'Crime',
        ]);

        Category::create([
            'name' => 'Music',
        ]);

        Category::create([
            'name' => 'Film',
        ]);
    }
}
