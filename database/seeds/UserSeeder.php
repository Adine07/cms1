<?php

use Illuminate\Database\Seeder;
use App\Model\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        User::create([
            'name' => 'admin',
            'email' => 'admin@admin',
            'password' => Hash::make('secret'),
            'role' => 'admin',
        ]);

        User::create([
            'name' => 'editor',
            'email' => 'editor@editor',
            'password' => Hash::make('secret'),
            'role' => 'editor',
        ]);

        User::create([
            'name' => 'author',
            'email' => 'author@author',
            'password' => Hash::make('secret'),
            'role' => 'author',
        ]);
    }
}
