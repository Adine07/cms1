@extends('layout.dash')

@section('title', 'Categories')
@section('head-script')
<style>
    label {
        white-space: nowrap;
    }

    .dataTables_filter {
        text-align: right;
    }

</style>
@endsection

@section('content')

<div class="card">
    <div class="card-header card-header-primary card-header-icon">
        <div class="card-icon"><i class="material-icons">assignment</i></div>
        <div class="d-flex justify-content-between mt-2">
            <h4 class="display-4 d-block text-dark">Categories</h4>
            <a href="/admin/categories/create" class="btn btn-primary">Create New Category</a>
        </div>
    </div>
    <div class="card-body">
        <table id="dtable" class="table table-striped table-no-bordered table-hover dataTable dtr-inline">
            <thead>
                <tr>
                    <th>Category Name</th>
                    <th width="30%">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($categories as $category )
                <tr>
                    <td>{{ $category->name }}</td>
                    <td>
                        <a href="{{ route('admin.categories.edit', ['category' => $category->id]) }}"
                            class="btn btn-warning btn-sm d-inline-block">Edit</a>
                        <form method="post" class="d-inline-block"
                            action="{{ route('admin.categories.destroy', $category->id) }}">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger btn-sm">Delete</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('end-script')
<script>
    $(document).ready(function () {
        $('#dtable').DataTable({
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }
        });

        var table = $('#datatable').DataTable();

        // Edit record
        table.on('click', '.edit', function () {
            $tr = $(this).closest('tr');
            var data = table.row($tr).data();
            alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
        });

        // Delete a record
        table.on('click', '.remove', function (e) {
            $tr = $(this).closest('tr');
            table.row($tr).remove().draw();
            e.preventDefault();
        });

        //Like record
        table.on('click', '.like', function () {
            alert('You clicked on Like button');
        });
    });

</script>
@endsection
