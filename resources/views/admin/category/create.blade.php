@extends('layout.dash')

@section('title', 'Create Category')

@section('content')

@if($errors->any())
<hr>
<ul>
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
</ul>
<hr>
@endif

<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Create New Category</h4>
                <p class="card-category">Create category for your article</p>
            </div>
            <div class="card-body">
                <form action="/admin/categories/" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label class="bmd-label-floating">Category Name</label>
                                <input type="text" class="form-control" value="{{ old('name') }}" name="name">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn py-2 btn-primary pull-right">Create</button>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection