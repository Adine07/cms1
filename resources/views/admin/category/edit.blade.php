@extends('layout.dash')

@section('title', 'Edit Category')

@section('content')

@if($errors->any())
<hr>
<ul>
	@foreach($errors->all() as $error)
	<li>{{ $error }}</li>
	@endforeach
</ul>
<hr>
@endif

<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Edit This Category</h4>
                <p class="card-category">Edit your category</p>
            </div>
            <div class="card-body">
                <form action="{{ route('admin.categories.update', ['category' => $categories->id]) }}" method="POST">
                    @csrf
					@method('PUT')
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label class="bmd-label-floating">Category Name</label>
                                <input type="text" class="form-control" name="name" value="{{ old('name', $categories->name) }}">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn py-2 btn-primary pull-right">Edit</button>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
