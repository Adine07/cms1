@extends('layout.dash')

@section('title', 'Dashboard')

@section('content')
<div class="row">
    <div class="col-md-4">
        <div class="card card-stats">
            <div class="card-header card-header-primary card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">article</i>
                </div>
                <p class="card-category">Articles</p>
                <h3 class="card-title">{{ $articles->count() }}

                </h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">update</i>
                    <p>Updated</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card card-stats">
            <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">category</i>
                </div>
                <p class="card-category">Categories</p>
                <h3 class="card-title">{{ $categories->count() }}

                </h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">update</i>
                    <p>Updated</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card card-stats">
            <div class="card-header card-header-danger card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">supervised_user_circle</i>
                </div>
                <p class="card-category">Users</p>
                <h3 class="card-title">{{ $users->count() }}

                </h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">update</i>
                    <p>Updated</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card card-stats">
            <div class="card-header card-header-info card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">turned_in</i>
                </div>
                <p class="card-category">Tag's</p>
                <h3 class="card-title">{{ $tags->count() }}

                </h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">update</i>
                    <p>Updated</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
