@extends('layout.dash')

@section('title', 'Create User')

@section('content')

@if($errors->any())
<hr>
<ul>
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
</ul>
<hr>
@endif

<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Create New User</h4>
                <p class="card-user">Create user for your article</p>
            </div>
            <div class="card-body">
                <form action="/admin/users/" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label class="bmd-label-floating">User Name</label>
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label class="bmd-label-floating">User E-Mail</label>
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label class="bmd-label-floating">User Password</label>
                                <input type="password" class="form-control" name="password">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <select name="role" class="form-control">
                                    <option value="">-- Select user Role --</option>
                                    <option value="admin">Admin</option>
                                    <option value="editor">Editor</option>
                                    <option value="author">Author</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn py-2 btn-primary pull-right">Create</button>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection