@extends('layout.dash')

@section('title', 'Create User')

@section('content')

@if($errors->any())
<hr>
<ul>
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
</ul>
<hr>
@endif

<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Create New User</h4>
                <p class="card-user">Create user for your article</p>
            </div>
            <div class="card-body">
                <form action="{{ route('admin.users.update', ['user' => $user->id]) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label class="bmd-label-floating">User Name</label>
                                <input type="text" class="form-control" name="name" value="{{ old('name', $user->name) }}">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label class="bmd-label-floating">User E-Mail</label>
                                <input type="email" class="form-control" name="email" value="{{ old('email', $user->email) }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <select name="role" class="form-control">
                                    <option value="">-- Select user Role --</option>
                                    <option value="admin" {{ $user->role == 'admin' ? 'selected' : '' }}>Admin</option>
                                    <option value="editor" {{ $user->role == 'editor' ? 'selected' : '' }}>Editor
                                    </option>
                                    <option value="author" {{ $user->role == 'author' ? 'selected' : '' }}>Author
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn py-2 btn-primary pull-right">Update</button>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection