@extends('layout.dash')

@section('title', 'Edit Article')

@section('head-script')
<script src="https://cdn.tiny.cloud/1/oixc359s0v223g7r2nfb661iddynhcunyatkpwv4f5tjp8dy/tinymce/5/tinymce.min.js"
    referrerpolicy="origin"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')

@if($errors->any())
<hr>
<ul>
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
</ul>
<hr>
@endif

<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Edit New Article</h4>
                <p class="card-article">Edit article for your blog</p>
            </div>
            <div class="card-body">
                <form action="{{ route('admin.articles.update', ['article' => $articles->id]) }}" method="POST">
                    @csrf
					@method('PUT')
                    <div class="row">
                        <div class="col">
                            <div class="form-group" style="margin-top: 43px;">
                                <label class="bmd-label-floating">Title</label>
                                <input type="text" class="form-control" name="title" value="{{ old('title', $articles->title) }}">
                            </div>
                        </div>

                        <div class="col">
                            <div class="form-group">
                                <label class="bmd-label-floating">Category</label>
                                <select class="form-control" name="category_id" id="">
                                    <option value="">-- Select Category --</option>
                                    @foreach($categories as $category)
                                    <option value="{{ $category->id }}" {{ $category->id == $articles->category_id ? 'selected' : '' }}>{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <textarea name="content" id="tiny">{{ old('content', $articles->content) }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="">Tags</label>
                                <select name="tag[]" multiple="multiple" class="form-control" id="tags">
                                    @foreach ($tags as $index => $tag)
                                        <option value="{{ $tag->id }}" {{ $articles->Tag()->pluck('tag_id')->contains($tag->id) ? 'selected': '' }}>{{ $tag->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="">
                                <label>Image
                                    <input type="file" name="photo" accept="image/*">
                                </label>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" value="{{ auth()->user()->id }}" name="user_id">
                    <button type="submit" class="btn py-2 btn-primary pull-right">Update</button>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection


@section('end-script')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script>
    tinymce.init({
        selector: 'textarea#tiny', // change this value according to your HTML
        plugin: 'a_tinymce_plugin',
        a_plugin_option: true,
        a_configuration_option: 400,
        height: 600,
    });

    $("#tags").select2({
        tags: true,
        tokenSeparators: [',', ' ']
    })

</script>
@endsection
