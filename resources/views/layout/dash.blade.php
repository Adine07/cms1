<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="/mtadmin/assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="/mtadmin/assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    @yield('title') | CMS1
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="/mtadmin/assets/css/material-dashboard.css?v=2.1.20" rel="stylesheet" />
  @yield('head-script')
</head>

<body class="">
  <div class="wrapper ">
    @include('layout.sidebar')
    <div class="main-panel">
      <!-- Navbar -->
      @include('layout.navbar')
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          @yield('content')
        </div>
      </div>
      <footer class="footer">
        @include('layout.footer')
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="/mtadmin/assets/js/core/jquery.min.js"></script>
  <script src="/mtadmin/assets/js/core/popper.min.js"></script>
  <script src="/mtadmin/assets/js/core/bootstrap-material-design.min.js"></script>
  <script src="/mtadmin/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!--  Plugin for Sweet Alert -->
  <script src="/mtadmin/assets/js/plugins/sweetalert2.js"></script>
  <!-- Forms Validations Plugin -->
  <script src="/mtadmin/assets/js/plugins/jquery.validate.min.js"></script>
  <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
  <script src="/mtadmin/assets/js/plugins/bootstrap-selectpicker.js"></script>
  <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
  <script src="/mtadmin/assets/js/plugins/jquery.dataTables.min.js"></script>
  <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
  <script src="/mtadmin/assets/js/plugins/jasny-bootstrap.min.js"></script>
  <!-- Library for adding dinamically elements -->
  <script src="/mtadmin/assets/js/plugins/arrive.min.js"></script>
  <!-- Chartist JS -->
  <script src="/mtadmin/assets/js/plugins/chartist.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="/mtadmin/assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="/mtadmin/assets/js/material-dashboard.js?v=2.1.2" type="text/javascript"></script>
  <!-- Material Dashboard DEMO methods, don't include it in your project! -->
  <script src="/mtadmin/assets/demo/demo.js"></script>
  @yield('end-script')
</body>

</html>