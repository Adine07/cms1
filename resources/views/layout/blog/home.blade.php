<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <!-- font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- font google -->
    <link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Serif:wght@700&display=swap" rel="stylesheet">

    <!-- slick Carousel -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css  ">

    <!-- MyStyle -->
    <link rel="stylesheet" href="/css/style.css">

    <title>@yield('title') | Blog News</title>
</head>

<body class="bg-light">

    <!-- header -->
    <header>
        <div class="mini-contact container d-flex justify-content-between py-3">
            <div class="d-flex">
                <p class="mr-2 mb-0"><i class="fa fa-mobile-phone fa-lg"></i> 0896-0123-0443</p>
                <p class="mb-0"><a href="mailto: adine@sharkstd.com" class="text-decoration-none"><i class="fa fa-envelope-o"></i>
                        Adine07</a></p>
            </div>
            <div class="sosmed text-lg block">
                <a class="ml-4 text-decoration-none" href="https://www.facebook.com/adi.pamungs"><i class="fa fa-lg fa-facebook-square"></i></a>
                <a class="ml-4 text-decoration-none" href="https://twitter.com/Adine_Pamungkas"><i class="fa fa-lg fa-twitter"></i></a>
                <a class="ml-4 text-decoration-none" href="https://www.instagram.com/adine_pamungkas/"><i class="fa fa-lg fa-instagram"></i></a>
                <a class="ml-4 text-decoration-none" href=""><i class="fa fa-lg fa-youtube-play"></i></a>
                <a class="ml-4 text-decoration-none" href=""><i class="fa fa-lg fa-linkedin-square"></i></a>
            </div>
        </div>
        <nav class="navbar navbar-expand-lg navbar-light bg-light" style="border-bottom: 1px solid rgba(0,0,0,0.1);">
            <div class="container py-2">
                <a class="navbar-brand" href="/"><span>New</span>s.io</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">News</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Blog</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Category
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                @foreach($categories as $cate)
                                <a class="dropdown-item" href="/categories/{{ $cate->id }}">{{ $cate->name }}</a>
                                @endforeach
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    <!-- jumbosell -->

    @yield('jumbosell')

    <!-- Popular Post -->

    @yield('populer')

    <!-- Main -->
    <div class="main container py-5">
        <div class="row">
            <div class="col-md-8 all-post">
                @yield('main')
            </div>
            <div class="col-md-4 sidebar">
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title text-bold m-1 ff">Social Links</h5>
                                <div class="sosmed d-flex">
                                    <div class="bg-link rounded-circle d-block m-1 p-4">
                                        <a class="text-decoration-none" href="https://www.facebook.com/adi.pamungs" target="_blank"><i class="fa fa-lg fa-facebook-square"></i></a>
                                    </div>
                                    <div class="bg-link rounded-circle d-block m-1 p-4">
                                        <a class="text-decoration-none" href="https://twitter.com/Adine_Pamungkas" target="_blank"><i class="fa fa-lg fa-twitter"></i></a>
                                    </div>
                                    <div class="bg-link rounded-circle d-block m-1 p-4">
                                        <a class="text-decoration-none" href="https://www.instagram.com/adine_pamungkas/" target="_blank"><i class="fa fa-lg fa-instagram"></i></a>
                                    </div>
                                    <div class="bg-link rounded-circle d-block m-1 p-4">
                                        <a class="text-decoration-none" href="" target="_blank"><i class="fa fa-lg fa-youtube-play"></i></a>
                                    </div>
                                    <div class="bg-link rounded-circle d-block m-1 p-4">
                                        <a class="text-decoration-none" href="" target="_blank"><i class="fa fa-lg fa-linkedin-square"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row py-3">
                    <div class="col">
                        <div class="card">
                            <div class="card-body m-1">
                                <h5 class="card-title text-bold ff">Recent Posts</h5>
                                @foreach ($recens as $recen )
                                <div class="card mb-3 my-1">
                                    <div class="row no-gutters">
                                        <div class="col-md-4 pr-1">
                                            <img src="/img/{{ $recen->image }}" class="w-100 mt-2" alt="...">
                                        </div>
                                        <div class="col-md-8">
                                            <div class="card-body p-0 m-0">
                                                <a href="/show/{{ $recen->id }}/{{ $recen->slug }}" class="post-side-title text-decoration-none text-dark ff">{{ $recen->title }}</a>
                                                <p class="date-post text-muted p-0 m-0">{{ $recen->created_at->format('F d Y') }}
                                                </p>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Footer -->
    <footer class="">
        <div class="container text-white ">
            <div class="row py-5 my-3">
                <div class="col-md-4">
                    <h3 class="ff">News.io</h3>
                    <p class="my-3">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea iusto similique dolorem.
                    </p>
                    <div class="sosmed text-lg block">
                        <a class="mr-4 text-decoration-none" href="https://www.facebook.com/adi.pamungs"><i class="fa fa-lg fa-facebook-square"></i></a>
                        <a class="mr-4 text-decoration-none" href="https://twitter.com/Adine_Pamungkas"><i class="fa fa-lg fa-twitter"></i></a>
                        <a class="mr-4 text-decoration-none" href="https://www.instagram.com/adine_pamungkas/"><i class="fa fa-lg fa-instagram"></i></a>
                        <a class="mr-4 text-decoration-none" href=""><i class="fa fa-lg fa-youtube-play"></i></a>
                        <a class="mr-4 text-decoration-none" href=""><i class="fa fa-lg fa-linkedin-square"></i></a>
                    </div>
                </div>
                <div class="col-md-4">
                    <h3 class="ff">Recent Posts</h3>
                    <div class="card mb-3 my-1">
                        <div class="row no-gutters">
                            <div class="col-md-4 pr-1">
                                <img src="https://asset.kompas.com/crops/6QlnEGqyAy4rFBoqrp5IQCKfdLY=/0x124:800x657/750x500/data/photo/2020/08/05/5f2a07fb3f2af.jpg" class="card-img" alt="...">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body p-0 m-0">
                                    <h5 class="post-side-title ff">PM Iran: Ledakan beirut perlu di
                                        selidiki lebih lanjut.</h5>
                                    <p class="date-post text-muted p-0 m-0">Agustus 07 2020</p>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card mb-3 my-1">
                        <div class="row no-gutters">
                            <div class="col-md-4 pr-1">
                                <img src="https://asset.kompas.com/crops/6QlnEGqyAy4rFBoqrp5IQCKfdLY=/0x124:800x657/750x500/data/photo/2020/08/05/5f2a07fb3f2af.jpg" class="card-img" alt="...">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body p-0 m-0">
                                    <h5 class="post-side-title ff">PM Iran: Ledakan beirut perlu di
                                        selidiki lebih lanjut.</h5>
                                    <p class="date-post text-muted p-0 m-0">Agustus 07 2020</p>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <h3 class="ff">Tags</h3>
                </div>
            </div>
            <hr>
            <div class="row text-center">
                <div class="col">
                    <p>&copy; 2020 Adine Pamungkas</p>
                </div>
            </div>
        </div>
    </footer>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous">
    </script>
    <!-- slick Carousel -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>

    <script>
        $(document).ready(function() {
            $('.slick').slick({
                dots: true,
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 4,
                autoplay: true,
                autoplaySpeed: 3000,
            });
        });
    </script>
</body>

</html>

<!--

-->