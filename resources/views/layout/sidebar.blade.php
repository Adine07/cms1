<?php

$dashboard = [
	'title' => 'Dashboard',
	'url' => '/admin',
  'model' =>App\Model\Home::class,
	'icon' => 'dashboard',
];

$category = [
	'title' => 'Categories',
	'url' => '/admin/categories',
  'model' =>App\Model\Category::class,
	'icon' => 'category',
];

$article = [
	'title' => 'Article',
	'url' => '/admin/articles',
  'model' =>App\Model\Article::class,
	'icon' => 'article',
];

$user = [
	'title' => 'User',
	'url' => '/admin/users',
  'model' =>App\Model\User::class,
	'icon' => 'person',
];

$menus = [$dashboard, $category, $article, $user];

$currentPath = '/' . request()->path();

?>
<div class="sidebar" data-color="purple" data-background-color="white" data-image="/mtadmin/assets/img/sidebar-1.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo"><a href="/admin" class="simple-text logo-normal">
          {{ auth()->user()->name }} | CMS
        </a></div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          @foreach ($menus as $index => $menu )
            @can('viewAny', $menu['model'])
              <li class="nav-item {{ $currentPath == $menu['url'] ? 'active' : '' }}">
                <a class="nav-link" href="{{ $menu['url'] }}">
                  <i class="material-icons">{{ $menu['icon'] }}</i>
                  <p>{{ $menu['title'] }}</p>
                </a>
              </li>
            @endcan
          @endforeach
        </ul>
      </div>
    </div>