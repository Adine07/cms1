<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="mtadmin/assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="mtadmin/assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>
        Register Page | CMS1
    </title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="mtadmin/assets/css/material-dashboard.min.css?v=2.1.2" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="mtadmin/assets/demo/demo.css" rel="stylesheet" />
    <style>
        body {
            background-image: url(https://images.pexels.com/photos/2312369/pexels-photo-2312369.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940);
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
        }
    </style>
</head>

<body class="off-canvas-sidebar">
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top text-white">
        <div class="container">
            <div class="navbar-wrapper">
                <a class="navbar-brand" href="#">CMS1 | Register Page</a>
            </div>
            <div class="collapse navbar-collapse justify-content-end">
                <ul class="navbar-nav">
                    <li class="nav-item ">
                        <a href="/login" class="nav-link">
                            <i class="material-icons">fingerprint</i>
                            Login
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a href="/" class="nav-link">
                            <i class="material-icons">lock_open</i>
                            Goto Blog
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->
    <div class="wrapper wrapper-full-page">
        <div class="page-header login-page header-filter" filter-color="black" style="background-image: url('mtadmin/assets/img/login.jpg'); background-size: cover; background-position: top center;">
            <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
            <div class="container">
                <div class="row pt-5">
                    <div class="col-lg-5 col-md-6 col-sm-8 ml-auto mr-auto">
                        <form class="form" method="post" action="/register">
                            @csrf
                            <div class="card card-login card-hidden">
                                <div class="card-header card-header-rose text-center">
                                    <h4 class="card-title">Register</h4>
                                </div>
                                <div class="card-body mt-4">
                                    <span class="bmd-form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="material-icons">face</i>
                                                </span>
                                            </div>
                                            <input type="text" name="name" class="form-control" value="{{ old('name') }}" placeholder="Name...">
                                        </div>
                                    </span>
                                    <span class="bmd-form-group d-block mt-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="material-icons">email</i>
                                                </span>
                                            </div>
                                            <input type="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="Email...">
                                        </div>
                                    </span>
                                    <span class="bmd-form-group d-block mt-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="material-icons">lock_outline</i>
                                                </span>
                                            </div>
                                            <input type="password" name="password" class="form-control" placeholder="Password...">
                                        </div>
                                    </span>
                                    <input type="hidden" name="role" value="author">
                                </div>
                                <div class="card-footer justify-content-center">
                                    <button class="btn btn-primary btn-round mt-4">Get Started</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container">
                    <div class="copyright m-auto">
                        &copy;
                        <script>
                            document.write(new Date().getFullYear())
                        </script>, made with <i class="material-icons">favorite</i> by
                        <a href="https://www.instagram.com/adine_pamungkas/" target="_blank">Adine Pamungkas</a> for a
                        better web.
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <script src="mtadmin/assets/js/core/bootstrap-material-design.min.js"></script>
    <script src="mtadmin/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
</body>

</html>