@extends('layout.blog.home')

@section('title', 'Category')

@section('main')
<div class="row">
    @foreach ($articles as $post )
    <div class="col-md-6 mb-5">
        <div class="card h-100">
            <img src="/img/{{ $post->image }}" class="card-img-top" height="220px" alt="...">
            <div class="card-body">
                <p class="badge badge-danger">{{ $post->category->name }}</p>
                <a href="/show/{{ $post->id }}/{{ $post->slug }}" class="text-dark text-decoration-none card-title text-bold ff">{{ $post->title }}</a>
                <p class="text-muted">{{ $post->created_at->format('F-Y')  }} | <i class="fa fa-bar-chart"></i>
                    {{ $post->counter }} views</p>
            </div>
        </div>
    </div>
    @endforeach
</div>
<div class="mx-auto text-center my-3">{{ $articles->links() }}</div>
@endsection