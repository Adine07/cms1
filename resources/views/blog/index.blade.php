@extends('layout.blog.home')

@section('title', 'Home')

@section('jumbosell')
<div id="carouselExampleCaptions" class="carousel slide carousel-fade" data-ride="carousel">
    <div class="carousel-inner">
        @foreach ($posts as $index=>$post )
        <div class="carousel-item {{ $index == $post->index ? 'active' : '' }}">
            <img src="/img/{{ $post->image }}" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block text-left">
                <p class="category font-weight-bold text-uppercase badge badge-danger d-inline-block">
                    {{ $post->category->name }}</p>
                <a href="/show/{{ $post->id }}/{{ $post->slug }}" class="d-block title ff text-white text-decoration-none">{{ $post->title }}</a>
                <p class="subtitle"> By <span style="color: salmon;">{{ $post->user->name }}</span> - <i class="fa fa-calendar"></i> {{ $post->created_at->format('Y-m-d') }}
                    - <i class="fa fa-bar-chart"></i> {{ $post->counter }} views</p>
            </div>
        </div>
        @endforeach
    </div>
    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
@endsection

@section('populer')
<div class="popular-post container my-5">
    <div class="row">
        <div class="col text-center">
            <h4>Popular Article</h4>
            <p class="text-muted">Don't miss to check out our most popular posts</p>
        </div>
    </div>
    <div class="row position-relative py-3 slick">
        @foreach ($hits as $hit )
        <div class="col-md-3 col" style="height: 300px">
            <div class="card position-absolute h-100">
                <img src="/img/{{ $hit->image }}" class="card-img-top" alt="...">
                <div class="card-body">
                    <p class="badge badge-danger">{{ $hit->category->name }}</p>
                    <a href="/show/{{ $hit->id }}/{{ $hit->slug }}" class="text-decoration-none text-dark card-title text-bold ff">{{ $hit->title }}</a>
                    <p class="text-muted">{{ $post->created_at->format('d-M-Y') }} | <i class="fa fa-bar-chart"></i>
                        {{ $hit->counter }} views</p>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection

@section('main')
<div class="row">
    @foreach ($posts as $post )
    <div class="col-md-6 mb-5">
        <div class="card h-100">
            <img src="/img/{{ $post->image }}" class="card-img-top" height="220px" alt="...">
            <div class="card-body">
                <p class="badge badge-danger">{{ $post->category->name }}</p>
                <a href="/show/{{ $post->id }}/{{ $post->slug }}" class="text-dark text-decoration-none card-title text-bold ff">{{ $post->title }}</a>
                <p class="text-muted">{{ $post->created_at->format('F-Y')  }} | <i class="fa fa-bar-chart"></i>
                    {{ $post->counter }} views</p>
            </div>
        </div>
    </div>
    @endforeach
</div>
<div class="mx-auto text-center my-3">{{ $posts->links() }}</div>
@endsection