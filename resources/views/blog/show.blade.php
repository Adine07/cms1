@extends('layout.blog.home')

@section('title', 'Home')

@section('main')
<div class="card mb-3">
    <img src="/img/{{ $post->image }}" class="card-img-top" alt="...">
    <div class="card-body">
        <span class="card-text" style="color: #59A5E4; font-weight: bold;">{{ $post->category->name }}</span>
        <br>
        <h5 class="card-title ff text-bold" style="font-size: 34px;">{{ $post->title }}</h5>
        <p class="card-text text-muted">By <span style="color: black;">{{ $post->user->name }}</span> |
            {{ $post->created_at->format('F d Y') }}</p>
        <div class="card-text">
            {!! $post->content !!}
        </div>
        <hr>
        <div class="tags">
            <div class="col">
                @foreach ($post->Tag as $ta)
                <span class="badge badge-secondary p-1" style="font-size: 12pt;">{{ $ta->name }}</span>
                @endforeach
            </div>
        </div>
        <hr>
        <div class="footer-card d-flex justify-content-between">
            <div class="comments col-md-6">
                <i class="fa fa-comment-o"></i> count Comments | <i class="fa fa-bar-chart"></i> {{ $post->counter }}
                Views
            </div>
            <div class="col-md-6 sosmed block">
                <p class="d-inline-block">Share: </p>
                <a class="ml-4 text-decoration-none" href="https://www.facebook.com/adi.pamungs"><i
                        class="fa fa-facebook-square"></i></a>
                <a class="ml-4 text-decoration-none" href="https://twitter.com/Adine_Pamungkas"><i
                        class="fa fa-twitter"></i></a>
                <a class="ml-4 text-decoration-none" href="https://www.instagram.com/adine_pamungkas/"><i
                        class="fa fa-instagram"></i></a>
                <a class="ml-4 text-decoration-none" href=""><i class="fa fa-youtube-play"></i></a>
                <a class="ml-4 text-decoration-none" href=""><i class="fa fa-linkedin-square"></i></a>
            </div>
        </div>
    </div>
</div>

<div class="card mb-3">
    <div class="card-body">
        <h5 class="card-title ff text-bold" style="font-size: 24px;">Leave Comment</h5>
        <form action="{{ route('comment.store', $post->id) }}" method="post">
            @csrf
            <label>Write comment</label>
            <textarea name="body" class="form-control"></textarea>

            <div class="form-group">
                <label class="">Name :</label>
                <input type="text" name="name" class="form-control">
            </div>
            <button class="btn btn-primary ml-auto d-block px-5">Comment</button>
        </form>
    </div>
</div>

<div class="card mb-3">
    <div class="card-body">
        <h5 class="card-title ff text-bold" style="font-size: 24px;">All Comments </h5>
    </div>
    @foreach ($comments as $comment)
    <div class="card-body">
        <h5 class="card-title ff text-bold" style="font-size: 20px;">{{ $comment->name }}</h5>
        <small class="text-muted">{{ $comment->created_at->format('d-M-Y') }}</small>
        <p class="card-text">{{ $comment->body }}</p>
    </div>
    @endforeach
</div>
@endsection
